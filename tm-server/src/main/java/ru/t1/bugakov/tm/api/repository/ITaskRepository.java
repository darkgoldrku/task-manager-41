package ru.t1.bugakov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.constacts.DBConstants;
import ru.t1.bugakov.tm.dto.model.TaskDTO;

import java.sql.SQLException;
import java.util.List;

public interface ITaskRepository extends IAbstractUserOwnedRepository<TaskDTO> {


    @Override
    @Insert("INSERT INTO " + DBConstants.TABLE_TASK + " (id, user_id, project_id, name, description, status, created) " +
            "VALUES (#{id},#{userId},#{projectId},#{name},#{description},#{status},#{created})")
    void add(@NotNull final TaskDTO task) throws SQLException;

    @Override
    @Select("SELECT * FROM tm_task WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    List<TaskDTO> findAll(final @NotNull @Param("userId") String userId) throws SQLException;

    @Override
    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} ORDER BY #{sortField}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    List<TaskDTO> findAllWithSort(final @NotNull @Param("userId") String userId, @Nullable final @Param("sortField") String sortField) throws SQLException;

    @Override
    @Select("SELECT COUNT(*) FROM tm_task WHERE user_id = #{userId}")
    int getSize(final @NotNull @Param("userId") String userId) throws SQLException;

    @Override
    @Select("SELECT * FROM tm_task WHERE id = #{id} AND user_id = #{userId} LIMIT 1")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    TaskDTO findById(final @NotNull @Param("userId") String userId, final @NotNull @Param("id") String id) throws SQLException;

    @Override
    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} LIMIT 1 OFFSET #{index}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    TaskDTO findByIndex(final @NotNull @Param("userId") String userId, final @NotNull @Param("index") Integer index) throws SQLException;

    @Select("SELECT EXISTS (SELECT id FROM tm_task WHERE user_id = #{userId} AND id = #{id})::boolean")
    boolean existsById(final @NotNull @Param("userId") String userId, final @NotNull @Param("id") String id) throws SQLException;

    @Override
    @Delete("DELETE FROM tm_task WHERE user_id = #{user_id}")
    void clear(final @NotNull @Param("user_id") String userId) throws SQLException;

    @Override
    @Delete("DELETE FROM tm_task WHERE user_id = #{user_id} AND id = #{id}")
    void remove(final @Nullable @Param("user_id") String userId, final @Nullable TaskDTO model) throws SQLException;

    @Update("UPDATE tm_task SET user_id = #{userId}, project_id = #{projectId}, name = #{name}, description = #{description}, status = #{status}, created = #{created} WHERE id = #{id}")
    void update(@NotNull TaskDTO task) throws SQLException;

    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} AND project_id = #{projectId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    List<TaskDTO> findAllByProjectId(@NotNull final @Param("userId") String userId, @NotNull final @Param("projectId") String projectId) throws SQLException;

}
