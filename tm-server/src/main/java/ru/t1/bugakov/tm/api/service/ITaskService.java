package ru.t1.bugakov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.dto.model.TaskDTO;
import ru.t1.bugakov.tm.enumerated.Status;

import java.sql.SQLException;
import java.util.List;

public interface ITaskService extends IAbstractUserOwnedService<TaskDTO> {

    @NotNull
    TaskDTO create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) throws SQLException;

    @NotNull
    List<TaskDTO> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) throws SQLException;

    @NotNull
    TaskDTO updateById(@Nullable final String userId, @Nullable final String id, @Nullable final String name, @Nullable final String description) throws SQLException;

    @NotNull
    TaskDTO updateByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final String name, @Nullable final String description) throws SQLException;

    @NotNull
    TaskDTO changeTaskStatusById(@Nullable final String userId, @Nullable final String id, @Nullable final Status status) throws SQLException;

    @NotNull
    TaskDTO changeTaskStatusByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final Status status) throws SQLException;

}
