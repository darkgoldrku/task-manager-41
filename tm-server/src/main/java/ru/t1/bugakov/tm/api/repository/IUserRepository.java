package ru.t1.bugakov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.constacts.DBConstants;
import ru.t1.bugakov.tm.dto.model.UserDTO;

import java.sql.SQLException;
import java.util.List;

public interface IUserRepository extends IAbstractRepository<UserDTO> {
    @Override
    @Insert("INSERT INTO  " + DBConstants.TABLE_USER + " (id, login, password_hash, email, first_name, last_name, middle_name, role, locked) " +
            "VALUES (#{id},#{login},#{passwordHash},#{email},#{firstName},#{lastName},#{middleName},#{role},#{locked})")
    void add(final @NotNull UserDTO model) throws SQLException;

    @Override
    @Select("SELECT * FROM tm_user")
    @Results(value = {
            @Result(property = "id", column = "id")
    })
    List<UserDTO> findAll() throws SQLException;

    @Override
    @Select("SELECT COUNT(*) FROM tm_user")
    int getSize() throws SQLException;

    @Override
    @Select("SELECT * FROM tm_user WHERE id = #{id} LIMIT 1")
    @Results(value = {
            @Result(property = "login", column = "login"),
            @Result(property = "locked", column = "locked"),
            @Result(property = "role", column = "role"),
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "email", column = "email"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    UserDTO findById(final @NotNull @Param("id") String id) throws SQLException;

    @Override
    @Select("SELECT * FROM tm_user LIMIT 1 OFFSET #{index}")
    @Results(value = {
            @Result(property = "login", column = "login"),
            @Result(property = "locked", column = "locked"),
            @Result(property = "role", column = "role"),
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "email", column = "email"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    UserDTO findByIndex(final @NotNull @Param("index") Integer index) throws SQLException;

    @Override
    @Delete("DELETE FROM tm_user")
    void clear() throws SQLException;

    @Override
    @Delete("DELETE FROM tm_user WHERE id = #{id}")
    void remove(final @NotNull UserDTO model) throws SQLException;

    @Update("UPDATE tm_user SET login = #{login}, password_hash = #{passwordHash}, email = #{email}, role = #{role}, locked = #{locked}, first_name = #{firstName}, last_name = #{lastName}, middle_name = #{middleName} WHERE id = #{id}")
    void update(@NotNull UserDTO user) throws SQLException;


    @Select("SELECT * FROM tm_user WHERE login = #{login} LIMIT 1")
    @Results(value = {
            @Result(property = "login", column = "login"),
            @Result(property = "locked", column = "locked"),
            @Result(property = "role", column = "role"),
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "email", column = "email"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    UserDTO findByLogin(@NotNull final @Param("login") String login) throws SQLException;

    @Select("SELECT * FROM tm_user WHERE email = #{email} LIMIT 1")
    @Results(value = {
            @Result(property = "login", column = "login"),
            @Result(property = "locked", column = "locked"),
            @Result(property = "role", column = "role"),
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "email", column = "email"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    UserDTO findByEmail(@NotNull final @Param("email") String email) throws SQLException;

    @Select("SELECT EXISTS (SELECT id FROM tm_user WHERE login = #{login})::boolean")
    boolean isLoginExist(@NotNull final @Param("login") String login) throws SQLException;

    @Select("SELECT EXISTS (SELECT id FROM tm_user WHERE email = #{email})::boolean")
    boolean isEmailExist(@NotNull final @Param("email") String email) throws SQLException;

    @Update("UPDATE tm_user SET locked = true WHERE login = #{login}")
    void lockUserByLogin(@NotNull final @Param("login") String login) throws SQLException;

    @Update("UPDATE tm_user SET locked = false WHERE login = #{login}")
    void unlockUserByLogin(@NotNull final @Param("login") String login) throws SQLException;

}
