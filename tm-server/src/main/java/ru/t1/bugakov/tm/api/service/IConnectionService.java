package ru.t1.bugakov.tm.api.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;

public interface IConnectionService {

    @NotNull
    @SneakyThrows
    SqlSession getSqlSession();

}
