package ru.t1.bugakov.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.dto.model.SessionDTO;
import ru.t1.bugakov.tm.dto.model.UserDTO;

import java.sql.SQLException;

public interface IAuthService {

    @NotNull
    UserDTO registry(@Nullable final String login, @Nullable final String password, @Nullable final String email) throws SQLException;

    @NotNull String login(@Nullable String login, @Nullable String password) throws SQLException;

    @NotNull
    @SneakyThrows
    SessionDTO validateToken(@Nullable String token);

    void invalidate(@Nullable SessionDTO session) throws SQLException;

}
