package ru.t1.bugakov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.dto.model.ProjectDTO;
import ru.t1.bugakov.tm.enumerated.Status;

import java.sql.SQLException;

public interface IProjectService extends IAbstractUserOwnedService<ProjectDTO> {

    @NotNull
    ProjectDTO create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) throws SQLException;

    @NotNull
    ProjectDTO updateById(@Nullable final String userId, @Nullable final String id, @Nullable final String name, @Nullable final String description) throws SQLException;

    @NotNull
    ProjectDTO updateByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final String name, @Nullable final String description) throws SQLException;

    @NotNull
    ProjectDTO changeProjectStatusById(@Nullable final String userId, @Nullable final String id, @Nullable final Status status) throws SQLException;

    @NotNull
    ProjectDTO changeProjectStatusByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final Status status) throws SQLException;

}
