package ru.t1.bugakov.tm.api.service;

import ru.t1.bugakov.tm.dto.model.SessionDTO;

public interface ISessionService extends IAbstractUserOwnedService<SessionDTO> {
}
