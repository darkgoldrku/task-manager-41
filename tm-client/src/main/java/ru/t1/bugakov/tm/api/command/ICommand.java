package ru.t1.bugakov.tm.api.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.SQLException;

public interface ICommand {

    void execute() throws SQLException;

    @NotNull
    String getName();

    @Nullable
    String getArgument();

    @NotNull
    String getDescription();

}
