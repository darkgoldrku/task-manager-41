package ru.t1.bugakov.tm.dto.response.system;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.dto.response.AbstractResponse;

@Getter
@Setter
public final class ServerVersionResponse extends AbstractResponse {

    @NotNull
    private String version;

}
