package ru.t1.bugakov.tm.dto.response.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.dto.model.UserDTO;

@NoArgsConstructor
public final class UserUpdateProfileResponse extends AbstractUserResponse {

    public UserUpdateProfileResponse(@Nullable final UserDTO user) {
        super(user);
    }

}
